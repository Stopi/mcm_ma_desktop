Readme
=====
This is part of my masters project. It's a fork of Jitsi with added functionality to support AR communication with a mobile client.

Jitsi
=====

Jitsi is a free open-source audio/video and chat communicator that supports protocols such as SIP, XMPP/Jabber, AIM/ICQ, IRC, Yahoo! and many other useful features.

Helpful Resources
-----------------
- https://jitsi.org
- [Download](https://download.jitsi.org) pre-built packages
- [Mailing lists](https://jitsi.org/Development/MailingLists)
 
Submitting Issues
-----------------
Before you open an issue, please discuss it on one of our [mailing lists](https://jitsi.org/Development/MailingLists).

Contributing
------------
Please, read the [contribution guidelines](CONTRIBUTING.md) before opening a new issue or pull request.