package net.java.sip.communicator.impl.gui.main.call.socketcommunication;

public interface RepaintInterface {
    void callRepaint();
    boolean isPainting();
}
