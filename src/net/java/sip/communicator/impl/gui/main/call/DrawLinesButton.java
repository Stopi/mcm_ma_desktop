package net.java.sip.communicator.impl.gui.main.call;

import net.java.sip.communicator.impl.gui.utils.ImageLoader;
import net.java.sip.communicator.service.protocol.Call;
import net.java.sip.communicator.service.resources.ImageID;

public class DrawLinesButton
		extends AbstractCallToggleButton
{

	private final UIVideoHandler2 uiVideoHandler;

	public DrawLinesButton(UIVideoHandler2 call)
	{
		this(call, false);
	}

	public DrawLinesButton(UIVideoHandler2 call, boolean selected)
	{
		super(
				null,
				true,
				selected,
				ImageLoader.DRAW_LINES_BUTTON,
				ImageLoader.DRAW_LINES_BUTTON_PRESSED,
				"service.gui.DRAW_LINES_BUTTON_TOOL_TIP");

		uiVideoHandler = call;
	}

	@Override
	public void buttonPressed()
	{
		uiVideoHandler.setDrawLinesVisible(
				!uiVideoHandler.isDrawLinesVisible());
	}
}
