package net.java.sip.communicator.impl.gui.main.call.socketcommunication;

import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MovementServer extends Thread {

    private ServerSocket serverSocket;
    private int port;
	private int factor;
    private double[][] rotationMatrix;
	private double[] orientation;
    private List<Point> points;
    private RepaintInterface component;
    private String mobileIp = "10.0.0.86";//"192.168.169.2"; //10.29.17.81;
    private int mobilePort = 8889;
    private boolean isRunning;

    public MovementServer(RepaintInterface component, int port)
    {
        this.component = component;
        this.port = port;
        rotationMatrix = new double[3][3];
        this.points = new ArrayList<Point>();
        isRunning = true;
	    this.factor = 32;
    }

    @Override
    public void run()
    {
        try
        {
            serverSocket = new ServerSocket(port);
            System.out.println("[DEBUG] Waiting for Client on port " + serverSocket.getLocalPort());
            while(isRunning) {
                //if(!component.isPainting()) {
                Socket server = serverSocket.accept();
                DataInputStream in = new DataInputStream(server.getInputStream());
                String json = in.readUTF();

                orientation = convertJsonToOrientation(json);

                /*if (!getPoints().isEmpty()) {
                    setPoints(transformPoints(getPoints()));

                    sendPointsToMobile(getPoints());

                    component.callRepaint();
                }*/
                in.close();
                server.close();
                //}
	            sendDrawingUpdateToMobile();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void sendPointsToMobile(List<Point> points) throws IOException {
        DrawingClient client = new DrawingClient(mobileIp, mobilePort);
        client.sendPointsToServer(points);
        System.out.println("[DEBUG] Points send to Server");
        client.closeConnection();
    }

	public void sendAugmentationToMobile(Augmentation augmentation) throws IOException {
		DrawingClient client = new DrawingClient(mobileIp, mobilePort);
		client.sendAugmentationToServer(augmentation);
		client.closeConnection();
	}

	public void sendDrawingUpdateToMobile() throws IOException
	{
		DrawingClient client = new DrawingClient(mobileIp, mobilePort);
		client.sendDrawingUpdate("UpdateDrawing");
		client.closeConnection();
	}

    private List<Point> transformPoints(List<Point> points)
    {
        List<Point> transPoints = new ArrayList<Point>();
	    System.out.println("[DEBUG] POINT BEFORE TRANS: x = " + points.get(0).getX() + " | y = " + points.get(0).getY());

        for(Point p : points) {
            double[] result = multiplyVectorWithMatrix(new double[] {p.getX(), p.getY(), 1}, rotationMatrix);

            transPoints.add(new Point((int) result[0], (int) result[1]));
        }
	    System.out.println("[DEBUG] POINT AFTER TRANS: x = " + transPoints.get(0).getX() + " | y = " + transPoints.get(0).getY());

        return transPoints;
    }

	/*private double[] calculateVectorWithRotationMatrix(double[] pointVector, double[][] rotaMatrix)
	{
		RealMatrix matrix = MatrixUtils.createRealMatrix(rotaMatrix);
		Vector3D vector = new Vector3D(pointVector);
		Rotation rotation = new Rotation(rotaMatrix, 1.0);

		return rotation.applyTo(vector).toArray();
	}*/

    private double[] multiplyVectorWithMatrix(double[] vector, double[][] matrix)
    {
        double[] result = new double[vector.length];
        double row;

        for(int i = 0; i < matrix.length; i++)
        {
            row = 0;
            for (int j = 0; j < matrix[i].length; j++)
            {
                row += (matrix[i][j] * vector[j]);
            }
            result[i] = row;
        }

        return result;
    }

    private double convertJsonToFloat(String json)
    {
        double value = 0;

        value = Double.valueOf(json);

        return value;
    }

    private double[][] convertJsonToMatrix(String json)
    {
        double[] matrix = new double[9];
        int i = 0;

        if(!json.startsWith("RotationMatrix"))
            return null;

        String array = json.substring(json.indexOf("[")+1, json.indexOf("]")-1);
        String[] splitted = array.split(";");

        for(String str : splitted)
        {
            matrix[i] = convertJsonToFloat(str);
            i++;
        }

        return new double[][] { {matrix[0], matrix[1], matrix[2]},
                               {matrix[3], matrix[4], matrix[5]},
                               {matrix[6], matrix[7], matrix[8]} };
    }

	private double[] convertJsonToOrientation(String json)
	{
		double[] orientation = new double[3];
		int i = 0;

		String array = json.substring(json.indexOf("[") + 1, json.indexOf("]") - 1);
		String[] splitted = array.split(";");

		for(String str : splitted)
		{
			orientation[i] = convertJsonToFloat(str);
			i++;
		}

		return orientation;
	}

    public double[][] getRotationMatrix() {
        return rotationMatrix;
    }

	public double[] getOrientation() {
		return orientation;
	}

	public double getAzimuth() {
		return orientation[Augmentation.AZIMUTH] * factor;
	}

	public double getPitch() {
		return orientation[Augmentation.PITCH] * factor;
	}

	public double getRoll() {
		return orientation[Augmentation.ROLL] * factor;
	}

    public void setRotationMatrix(double[][] rotationMatrix) {
        this.rotationMatrix = rotationMatrix;
    }

    public synchronized List<Point> getPoints() {
        return points;
    }

    public synchronized void setPoints(List<Point> points) {
        this.points = points;
    }

    public synchronized void addPoint(Point point) {
        this.points.add(point);
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setIsRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }
}
