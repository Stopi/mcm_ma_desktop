package net.java.sip.communicator.impl.gui.main.call.socketcommunication;


import java.awt.Point;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class DrawingClient {

    private int port;
    private String address;
    private Socket server;

    public DrawingClient (String address, int port) throws IOException {
        this.address = address;
        this.port = port;
        server = new Socket(address, port);
    }

    public boolean sendPointsToServer(List<Point> points) throws IOException {
        boolean result = false;

        DataOutputStream out = new DataOutputStream(server.getOutputStream());
        out.writeUTF(convertPointListToJson(points));

        return result;
    }

	public void sendAugmentationToServer(Augmentation augmentation) throws IOException
	{
		//boolean result = false;

		DataOutputStream out = new DataOutputStream(server.getOutputStream());
		out.writeUTF(augmentation.toJson());

		//return result;
	}

    private String convertPointListToJson(List<Point> points)
    {
        StringBuilder sb = new StringBuilder();

        for (Point p : points)
        {
            sb.append("x=").append(String.valueOf(p.x));
            sb.append(";y=").append(String.valueOf(p.y)).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));

        return sb.toString();
    }

    public void closeConnection() throws IOException
    {
        server.close();
    }

	public void sendDrawingUpdate(String message) throws IOException
	{
		DataOutputStream out = new DataOutputStream(server.getOutputStream());
		out.writeUTF(message);
	}
}
