package net.java.sip.communicator.impl.gui.main.call.socketcommunication;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Augmentation
{
	public static int AZIMUTH = 0;
	public static int PITCH = 1;
	public static int ROLL = 2;

	private double[] orientation;
	private List<Point> points;

	public Augmentation(double azimuth, double pitch, double roll)
	{
		this.orientation = new double[3];
		this.orientation[AZIMUTH] = azimuth;
		this.orientation[PITCH] = pitch;
		this.orientation[ROLL] = roll;
		points = new ArrayList<Point>();
	}

	public double[] getOrientation()
	{
		return orientation;
	}

	public void setOrientation(double[] orientation)
	{
		this.orientation = orientation;
	}

	public List<Point> getPoints()
	{
		return points;
	}

	public void setPoints(List<Point> points)
	{
		this.points = points;
	}

	public void addPoint(Point point)
	{
		this.points.add(point);
	}

	public String toJson()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Augmentation={");
		sb.append(convertOrientationToString(getOrientation()));
		sb.append(convertPointListToJson(points));
		sb.append("}");

		return sb.toString();
	}

	private String convertOrientationToString(double[] orientation)
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Orientation={");
		sb.append("azimuth:").append(orientation[AZIMUTH]);
		sb.append(", pitch:").append(orientation[PITCH]);
		sb.append(", roll:").append(orientation[ROLL]);
		sb.append("},");

		return sb.toString();
	}

	private String convertPointListToJson(List<Point> points)
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Points=[");

		for (Point p : points)
		{
			sb.append("{x:").append(String.valueOf(p.x));
			sb.append(", y:").append(String.valueOf(p.y)).append("},");
		}
		sb.deleteCharAt(sb.lastIndexOf(","));

		sb.append("]");

		return sb.toString();
	}
}
